import React from 'react';
import "./Container.css";
import {useDispatch, useSelector} from "react-redux";
import {changeDecode, changeEncode, changePassword, sendMessage} from "../store/actions";

const Container = () => {
    const dispatch = useDispatch();
    const {encode, decode, password} = useSelector(state => state);

    const handleEncodeChange = e => {
        dispatch(changeEncode(e.target.value));
    };

    const handleDecodeChange = e => {
        dispatch(changeDecode(e.target.value));
    };

    const handlePasswordChange = e => {
        dispatch(changePassword(e.target.value))
    }

    const handleClickBtn = type => {
        let sendObj = {
            message: "",
            password: password,
        };
        if (type === "encode") {
            sendObj.message = encode;
        } else if (type === "decode") {
            sendObj.message = decode
        }
        if (sendObj.message.length === 0 || password.length === 0) {
            console.log("Input can't be empty");
        } else {
            dispatch(sendMessage(sendObj, type));
        }
    }

    return (
        <div className="Container">
            <form action="" onSubmit={e => e.preventDefault()}>
                <input
                    onChange={handlePasswordChange}
                    value={password}
                    id="password"
                    type="text"
                    name="password"
                    placeholder="Password..."
                />
                <label htmlFor="encodeInp">Encode</label>
                <input
                    onChange={handleEncodeChange}
                    value={encode}
                    type="text"
                    name="encode"
                    placeholder="Encode..."
                    id="encodeInp"
                />
                <div className="btn-row">
                    <button onClick={() => handleClickBtn("encode")}>Encode</button>
                    <button onClick={() => handleClickBtn("decode")}>Decode</button>
                </div>
                <label htmlFor="decodeInp">Decode</label>
                <input
                    onChange={handleDecodeChange}
                    value={decode}
                    type="text"
                    name="decode"
                    placeholder="Decode..."
                    id="decodeInp"
                />
            </form>
        </div>
    );
};

export default Container;