import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Container from "./containers/Container";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Container}/>
          <Route render={() => <h1>Not found</h1>}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
