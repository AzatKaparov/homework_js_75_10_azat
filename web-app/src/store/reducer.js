import {CHANGE_DECODE, CHANGE_ENCODE, CHANGE_PASSWORD} from "./actions";

const initialState = {
    loading: false,
    encode: "",
    decode: "",
    password: "",
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case CHANGE_ENCODE:
            return {...state, encode: action.value};
        case CHANGE_DECODE:
            return {...state, decode: action.value};
        case CHANGE_PASSWORD:
            return {...state, password: action.value};
        default:
            return state;
    }
};

export default reducer;
