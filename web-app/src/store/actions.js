import axios from "axios";

export const CHANGE_ENCODE = "CHANGE_ENCODE";
export const CHANGE_DECODE = "CHANGE_DECODE";
export const CHANGE_PASSWORD = "CHANGE_PASSWORD";

export const changeEncode = value => ({type: CHANGE_ENCODE, value: value});
export const changeDecode = value => ({type: CHANGE_DECODE, value: value});
export const changePassword = value => ({type: CHANGE_PASSWORD, value: value});

export const sendMessage = (message, type) => {
    return async dispatch => {
        try {
            const response = await axios.post(`http://localhost:4321/cypher/${type}`, message);
            if (type === "encode") {
                dispatch(changeEncode(response.data));
            } else if (type === "decode") {
                dispatch(changeDecode(response.data));
            }
        } catch (e) {
            console.error(e);
        }
    }
}