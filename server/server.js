const express = require('express');
const cors = require('cors');
const app = express();
const coders = require('./app/coders');
const port = 4321;


app.use(express.json());
app.use(cors());
app.use('/cypher', coders);

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});
