const express = require("express");
const router = express.Router();
const Vigenere = require('caesar-salad').Vigenere;


router.post("/encode", (req, res) => {
    const {password, message} = req.body;
    const response = Vigenere.Cipher(password).crypt(message);
    res.send(response);
});

router.post("/decode", (req, res) => {
    const {password, message} = req.body;
    const response = Vigenere.Decipher(password).crypt(message);
    res.send(response);
});

module.exports = router;